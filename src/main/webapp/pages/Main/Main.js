Application.$controller("MainPageController", ["$scope", "$websocket", function($scope, $websocket) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        // subscribe to our queue
        var sqs = new AWS.SQS({
            endpoint: 'https://sqs.eu-west-1.amazonaws.com/466286235171/wmsdktest'
        });
        var params = {
            QueueUrl: 'https://sqs.eu-west-1.amazonaws.com/466286235171/wmsdktest',
        };
        sqs.receiveMessage(params, function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else console.log(data); // successful response
        });
    };

    $scope.wstextBlur = function($event, $isolateScope) {
        // // Open a WebSocket connection
        var ws = $websocket('wss://echo.websocket.org');
        ws.send($isolateScope.datavalue);
        alert('Sent!');

        ws.onMessage(function(message) {
            console.log(message);
            console.log(message.data);
            alert('Got ' + message.data);
            $scope.Variables.st_socketReply.setData({
                dataValue: message.data
            });
        });
    };



}]);

Application.$controller("grid1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("grid3Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

// Application.module('YOUR_APP', [
//         'ngWebSocket' // you may also use 'angular-websocket' if you prefer
//     ])
//     //                          WebSocket works as well
//     .factory('MyData', function($websocket) {
//         // Open a WebSocket connection
//         var dataStream = $websocket('wss://echo.websocket.org');

//         var collection = [];

//         dataStream.onMessage(function(message) {
//             collection.push(JSON.parse(message.data));
//         });

//         var methods = {
//             collection: collection,
//             get: function() {
//                 dataStream.send(JSON.stringify({
//                     action: 'get'
//                 }));
//             }
//         };

//         return methods;
//     })
//     .controller('MainPageController', function($scope, MyData) {
//         $scope.MyData = MyData;
//     });

Application.$controller("grid4Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);