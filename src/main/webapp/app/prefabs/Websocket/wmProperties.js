var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "defaultLanguage" : "en",
  "displayName" : "Websocket",
  "homePage" : "Main",
  "name" : "Websocket",
  "platformType" : "DEFAULT",
  "supportedLanguages" : "en",
  "type" : "PREFAB",
  "version" : "1.0"
};