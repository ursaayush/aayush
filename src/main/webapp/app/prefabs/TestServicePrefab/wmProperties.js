var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "defaultLanguage" : "en",
  "displayName" : "TestServicePrefab",
  "homePage" : "Main",
  "name" : "TestServicePrefab",
  "platformType" : "DEFAULT",
  "supportedLanguages" : "en",
  "type" : "PREFAB",
  "version" : "1.0"
};