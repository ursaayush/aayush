package com.aayush.commons;

import org.springframework.stereotype.Component;

@Component
public class CommonFunction {

    public Integer getSumFancy(int x, int y){
        System.out.println("Inside Fancy Service!");
        return x+y;
    }
    
    // This is just a test comment

}
